package de.mo.DrawCanvas;

import org.vaadin.hezamu.canvas.Canvas.CanvasMouseMoveListener;

import com.vaadin.shared.MouseEventDetails;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;

import de.mo.DrawCanvas.helper.Path;
import de.mo.DrawCanvas.helper.VaadinCanvas;
import de.mo.DrawCanvas.helper.VaadinCanvas.VaadinCanvasMouseDownListener;
import de.mo.DrawCanvas.helper.VaadinCanvas.VaadinCanvasMouseUpListener;
import reactive.VaadinObservables;
import rx.Observable;

public class DrawView  extends HorizontalLayout{
	
	private static final long serialVersionUID = 8341409499458846292L;
	
	private VaadinCanvas canvas;

    public DrawView(MyUI ui) {
        setStyleName("main-screen");

        Panel viewContainer = new Panel();
        viewContainer.setWidth(648, Unit.PIXELS);
        viewContainer.setHeight(480, Unit.PIXELS);
        viewContainer.setSizeFull();
        addComponent(viewContainer);
        setExpandRatio(viewContainer, 1);
        
        viewContainer.setContent(canvas = new VaadinCanvas());
        canvas.setWidth(648, Unit.PIXELS);
        canvas.setHeight(480, Unit.PIXELS);
        canvas.
        
        setSizeFull();
    }
    
    public void addMouseDownListener(VaadinCanvasMouseDownListener listener) {
    	this.canvas.addMouseDownListener(listener);
    }
    
    public void addMouseMoveListener(CanvasMouseMoveListener listener) {
    	this.canvas.addMouseMoveListener(listener);
    }

    public void addMouseUpListener(VaadinCanvasMouseUpListener listener) {
    	this.canvas.addMouseUpListener(listener);
    }
    
    public Observable<MouseEventDetails> getMouseMoveEvents() {
    	return VaadinObservables.fromCanvasMouseMoveEvent(this.canvas);
    }
    
    public Observable<MouseEventDetails> getMouseDownEvents() {
    	return VaadinObservables.fromCanvasMouseDownEvent(this.canvas);
    }
    
    public Observable<MouseEventDetails> getMouseUpEvents() {
    	return VaadinObservables.fromCanvasMouseUpEvent(this.canvas);
    }
    
    public void drawLine(double x, double y) {
    	this.canvas.lineTo(x, y);
    }
    
    public void moveTo(double x, double y) {
    	this.canvas.moveTo(x, y);
    }
    
    public VaadinCanvas getCanvas() {
    	return canvas;
    }

	public void removeMouseMoveListener(CanvasMouseMoveListener moveListener) {
		this.canvas.removeListener(moveListener);
	}

	public void drawPath(Path path) {
		path.getPoints().forEach(point -> {
			if(path.getPoints().indexOf(point) == 0) {
				moveTo(point.getX(), point.getY());
			} else {
				drawLine(point.getX(), point.getY());
			}
		});
		this.canvas.stroke();
	}

}
