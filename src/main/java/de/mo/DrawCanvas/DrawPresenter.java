package de.mo.DrawCanvas;


import com.vaadin.shared.MouseEventDetails;

import de.mo.DrawCanvas.helper.Path;
import rx.Observable;
import rx.functions.Action0;

public class DrawPresenter {
	
	DrawView view;
	
	Path path = null;
	
	public DrawPresenter(DrawView view) {
		this.view = view;
		
//		Path path = null;
//		CanvasMouseMoveListener moveListener = moveEvent -> {
//			path.lineTo(moveEvent.getRelativeX(), moveEvent.getRelativeY());
//			this.view.drawPath(path);
//		};
//		
//		this.view.addMouseDownListener(event -> {
//			path = new Path(event.getRelativeX(), event.getRelativeY());
//			this.view.addMouseMoveListener(moveListener);
//		});
//		
//		this.view.addMouseUpListener(event -> {
//			this.view.removeMouseMoveListener(moveListener);
//			path.close();
//	    	this.view.drawPath(path);
//		});
		
		
		Observable<MouseEventDetails> mouseDown =  view.getMouseDownEvents();
		Observable<MouseEventDetails> mouseUp =  view.getMouseUpEvents();
		Observable<MouseEventDetails> mouseMove =  view.getMouseMoveEvents();
		
		Observable<Path> pathObservable = mouseDown.flatMap(event -> {
			Path path = new Path(event.getRelativeX(), event.getRelativeY());
			
			return mouseMove.map(moveEvent -> {
				path.lineTo(moveEvent.getRelativeX(), moveEvent.getRelativeY());
				return path;
			})
			.takeUntil(mouseUp)
			.doOnCompleted(new Action0() {

				@Override
				public void call() {
					path.close();
				}
			});
			
		});
		pathObservable.subscribe(path -> view.drawPath(path));
				
	}

}
