package de.mo.DrawCanvas.helper;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class Path {
	
	private List<Point> points;
	
	private boolean closed;
	
	public Path(int x, int y) {
		points = new ArrayList<Point>();
	}
	
	public void lineTo(int x, int y) {
		if(!closed) {
			this.points.add(new Point(x, y));
		}
	}
	
	public List<Point> getPoints() {
		return this.points;
	}

	public void close() {
		this.closed = true;
	}
	
	public boolean isClosed() {
		return closed;
	}

}
