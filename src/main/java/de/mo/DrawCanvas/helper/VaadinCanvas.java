package de.mo.DrawCanvas.helper;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.vaadin.hezamu.canvas.Canvas;
import org.vaadin.hezamu.canvas.client.ui.CanvasServerRpc;

import com.vaadin.shared.MouseEventDetails;

public class VaadinCanvas extends Canvas {
	
	public interface VaadinCanvasMouseDownListener {
		public void onMouseDown(MouseEventDetails med);
	}
	
	public interface VaadinCanvasMouseUpListener {
		public void onMouseUp(MouseEventDetails med);
	}
	
	private final List<CanvasMouseMoveListener> mouseMoveListeners = new CopyOnWriteArrayList<CanvasMouseMoveListener>();
	private final List<VaadinCanvasMouseDownListener> mouseDownListeners = new CopyOnWriteArrayList<VaadinCanvasMouseDownListener>();
	private final List<VaadinCanvasMouseUpListener> mouseUpListeners = new CopyOnWriteArrayList<VaadinCanvasMouseUpListener>();	
	
	public VaadinCanvas() {
		 registerRpc(new CanvasServerRpc() {
             private static final long serialVersionUID = 1L;

             @Override
             public void mouseUp(MouseEventDetails med) {
            	 fireMouseUp(med);
             }

             @Override
             public void mouseMoved(MouseEventDetails med) {
 				fireMouseMove(med);
             }

             @Override
             public void mouseDown(MouseEventDetails med) {
            	 fireMouseDown(med);
             }

             @Override
             public void imagesLoaded() {
             }
         });
	}

	/**
	 * Adds a CanvasMouseMoveListener.
	 *
	 * @param listener
	 *            the listener
	 */
	public void addMouseMoveListener(CanvasMouseMoveListener listener) {
		if (!mouseMoveListeners.contains(listener)) {
			mouseMoveListeners.add(listener);
			getState().listenMouseMove = true;
		}
	}

	/**
	 * Removes a CanvasMouseMoveListener.
	 *
	 * @param listener
	 *            the listener
	 */
	public void removeListener(CanvasMouseMoveListener listener) {
		if (mouseMoveListeners.contains(listener)) {
			mouseMoveListeners.remove(listener);
		}
	}

	private void fireMouseMove(MouseEventDetails mouseDetails) {
		for (CanvasMouseMoveListener listener : mouseMoveListeners) {
			listener.onMove(mouseDetails);
		}
	}
	
	/**
	 * Adds a CanvasMouseDownListener.
	 *
	 * @param listener
	 *            the listener
	 */
	public void addMouseDownListener(VaadinCanvasMouseDownListener listener) {
		if (!mouseDownListeners.contains(listener)) {
			mouseDownListeners.add(listener);
		}
	}

	/**
	 * Removes a CanvasMouseDownListener.
	 *
	 * @param listener
	 *            the listener
	 */
	public void removeListener(CanvasMouseDownListener listener) {
		if (mouseDownListeners.contains(listener)) {
			mouseDownListeners.remove(listener);
		}
	}

	private void fireMouseDown(MouseEventDetails med) {
		for (VaadinCanvasMouseDownListener listener : mouseDownListeners) {
			listener.onMouseDown(med);
		}
	}
	
	/**
	 * Adds a CanvasMouseUpListener.
	 *
	 * @param listener
	 *            the listener
	 */
	public void addMouseUpListener(VaadinCanvasMouseUpListener listener) {
		if (!mouseUpListeners.contains(listener)) {
			mouseUpListeners.add(listener);
		}
	}

	/**
	 * Removes a CanvasMouseUpListener.
	 *
	 * @param listener
	 *            the listener
	 */
	public void removeListener(CanvasMouseUpListener listener) {
		if (mouseUpListeners.contains(listener)) {
			mouseUpListeners.remove(listener);
		}
	}

	private void fireMouseUp(MouseEventDetails med) {
		for (VaadinCanvasMouseUpListener listener : mouseUpListeners) {
			listener.onMouseUp(med);
		}
	}

	public void removeListener(VaadinCanvasMouseUpListener listener) {
		this.mouseUpListeners.remove(listener);
	}

	public void removeListener(VaadinCanvasMouseDownListener listener) {
		this.mouseDownListeners.remove(listener);
	}
	 
}
