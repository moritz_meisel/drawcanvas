package reactive;

import org.vaadin.hezamu.canvas.Canvas;
import org.vaadin.hezamu.canvas.Canvas.CanvasMouseMoveListener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Property.ValueChangeNotifier;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;

import de.mo.DrawCanvas.helper.VaadinCanvas;
import de.mo.DrawCanvas.helper.VaadinCanvas.VaadinCanvasMouseDownListener;
import de.mo.DrawCanvas.helper.VaadinCanvas.VaadinCanvasMouseUpListener;
import rx.Observable;
import rx.functions.Action0;
import rx.subscriptions.Subscriptions;

public class VaadinObservables {
	
	public static Observable<ValueChangeEvent> fromValueChangeEvent(ValueChangeNotifier changeNotifier) {
		return Observable.<ValueChangeEvent>create(subscriber -> {
			ValueChangeListener listener =  event -> subscriber.onNext(event);
			
			changeNotifier.addValueChangeListener(listener);
			subscriber.add(Subscriptions.create(new Action0() {

				@Override
				public void call() {
					changeNotifier.removeValueChangeListener(listener);
				}
				
			}));
			
		});
	}
	
	public static Observable<MouseEventDetails> fromCanvasMouseDownEvent(VaadinCanvas canvas) {
		return Observable.<MouseEventDetails>create(subscriber -> {
			VaadinCanvasMouseDownListener listener = med -> subscriber.onNext(med);
			
			canvas.addMouseDownListener(listener);
			subscriber.add(Subscriptions.create(new Action0() {

				@Override
				public void call() {
					canvas.removeListener(listener);
				}
				
			}));
			
		});
	}
	
	public static Observable<MouseEventDetails> fromCanvasMouseUpEvent(VaadinCanvas canvas) {
		return Observable.<MouseEventDetails>create(subscriber -> {
			VaadinCanvasMouseUpListener listener = new VaadinCanvasMouseUpListener() {
				
				@Override
				public void onMouseUp(MouseEventDetails med) {
					subscriber.onNext(med);
				}
			};
			
			canvas.addMouseUpListener(listener);
			subscriber.add(Subscriptions.create(new Action0() {

				@Override
				public void call() {
					canvas.removeListener(listener);
				}
				
			}));
		});
	}
	
	public static Observable<MouseEventDetails> fromCanvasMouseMoveEvent(Canvas canvas) {
		return Observable.<MouseEventDetails>create(subscriber -> {
			CanvasMouseMoveListener listener = new CanvasMouseMoveListener() {

				@Override
				public void onMove(MouseEventDetails mouseDetails) {
					subscriber.onNext(mouseDetails);
				}
				
			};
			
			canvas.addMouseMoveListener(listener);
			subscriber.add(Subscriptions.create(new Action0() {

				@Override
				public void call() {
					canvas.removeListener(listener);
				}
				
			}));
			
		});
	}

	public static Observable<ClickEvent> fromClickEvent(Button submitButton) {
		return Observable.<ClickEvent>create(subscriber -> {
			@SuppressWarnings("serial")
			ClickListener listener = new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					subscriber.onNext(event);
				}
			};
			
			submitButton.addClickListener(listener);
			subscriber.add(Subscriptions.create(new Action0() {

				@Override
				public void call() {
					submitButton.removeClickListener(listener);
				}
				
			}));
			
		});
	}

	public static Observable<ItemClickEvent> fromItemClickEvent(Grid grid) {
		return Observable.<ItemClickEvent>create(subscriber -> {
			@SuppressWarnings("serial")
			ItemClickListener listener = new ItemClickListener() {
				
				@Override
				public void itemClick(ItemClickEvent event) {
					subscriber.onNext(event);
				}

			};
			
			grid.addItemClickListener(listener);
			subscriber.add(Subscriptions.create(new Action0() {

				@Override
				public void call() {
					grid.removeItemClickListener(listener);
				}
				
			}));
			
		});
	}
	

}
